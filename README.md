# Language Production in Response to Polar Questions

R-Code (c) 2017-18, Phillip M. Alday and Caitlin Decuyper.

This repository contains the code and processed data used in:

Meyer, Antje, Phillip M. Alday, Caitlin Decuyper and Birgit Knudsen (2018): [Working together: Contributions of corpus analyses and experimental psycholinguistics to understanding conversation](https://doi.org/10.3389/fpsyg.2018.00525). Frontiers in Psychology.

The published analysis can be found in `analysis.R` (largely frequentist with a Bayesian null-hypothesis "acceptance" for the supplemtnary materials).
A nearly equivalent Bayesian variant of the frequentist analysis can be found in `bayes.R`.
Both analyses were developed in parallel before initial manuscript submission.
As the Bayesian analysis was developed largely for tutorial / comparison purposes, it was not further fine-tuned in the review process (although that generally impacted graphical presentation and not modelling choices).